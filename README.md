# Lokales Setup (Docker)

## Installieren von Docker
Zunächst muss **docker** und **docker-compose** auf der lokalen Maschine installiert sein. Unter windows muss dazu ggf. ein Linux subsystem installiert werden (siehe https://docs.microsoft.com/de-de/windows/wsl/install-win10)

Daneben wird auch docker-compose benötigt (In "Docker for Windows" bereits enthalten). Im Projekt genutzte Versionen:

- docker-compose version 1.27.4
- docker version 20.10.2

Die korrekte Installation und Version der Services kann folgendermaßen überprüft werden: 

```
docker --version
docker-compose --version
````

## Einloggen in private Container-Registry

Als nächstes muss eine Verbindung zur privaten Container-Registry des Projekts hergestellt werden. Ein personalisierter Zugang mit Username und Password kann beim PO angefragt werden.
```
docker login registry.gitlab.com/meinhp/ -u [Username] -p [Password]
```
## Klonen dieses Repositories

Im Anschluss muss die **docker-compose.yml** aus diesem Repository heruntergeladen werden. Dazu empfiehlt es sich, das Repository zu klonen. So können zukünftige Änderungen am Deployment leicht aktualisiert werden. 

```
git clone https://gitlab.com/meinhp/meinhp.git
```

# Starten der Services

Dazu muss zunächst mit einer CMD/Terminal/Powershell/Kommandozeile in den Projektordner navigiert werden, sodass die **docker-compose.yml im aktuellen Arbeitsverzeichnis** liegt. Danach kann das Projekt mit 
```
docker-compose up
``` 

gestartet werden. Mit der Tastenkombination `STRG+C` wird ein Shutdown-Kommando an alle Komponenten des Stacks gesendet.

Alternativ kann das Projekt ohne Log-Output gestartet werden:
```
docker-compose up -d
``` 
Das Projekt wird anschließend folgendermaßen gestoppt werden: 
```
docker-compose down
``` 

# Testen der Anwendung

Die Anwendung kann unter 

[http://localhost:4200](http://localhost:4200) 

direkt ausprobiert werden. Nach jedem Neustart wird die Datenbank mit neuen Testdaten befüllt und auf einen sauberen Stand zurückgesetzt.

# Aktualisierung der lokalen Anwendung

```
git pull
```

```
docker-compose pull && docker-compose up // Mit Logging
```

```
docker-compose pull && docker-compose up -d // Ohne Logging
````

# Bekannte Probleme [WIP]

- Beim erstmaligen Starten des Stacks werden einige Fehlermeldungen angezeigt, die mit der erstmaligen Initialisierung der Datenbank zusammenhängen. Diese beeinflussen die Funktionsweise des Stacks nicht und können vorerst ignoriert werden.   